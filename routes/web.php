<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PiplocationController@index')->name("pip.index");


Route::get('/create', 'PiplocationController@create')->name("pip.create");
Route::get('/index', 'PiplocationController@index')->name("pip.index");
Route::get('/details', 'PiplocationController@details')->name("pip.details");
Route::post('/store', 'PiplocationController@store')->name("pip.store");
Route::get('/getlocations', 'PiplocationController@getLocations')->name("pip.getlocations");







