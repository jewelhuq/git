



function ajaxMessage(url,data,onSuccessFunction,type="POST",onFailedFunction="")
{
    $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: "json",
        success: function (result)
        {
            if(result.success)
            {
                eval(onSuccessFunction);
            }
            else
            {
                if(onFailedFunction=="")
                {
                    swal("Something Wrong","Something Went Wrong","warning");
                }
                else
                {
                    eval(onFailedFunction);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError)
        {
            alert(xhr.status);
            alert(thrownError);
        }
    });


}
