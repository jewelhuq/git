<?php

namespace App\Http\Controllers;

use App\LocationData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PiplocationController extends Controller
{
    public function store(Request $request)
    {


        try {

            $locationData = new LocationData();
            $locationData->pip_name = $request->pip_name;
            $locationData->creator_name = $request->creator_name;
            $locationData->department = $request->department;
            $locationData->created_password = $request->created_password;
            $locationData->latitude = $request->lat;
            $locationData->longitude = $request->lng;
            $locationData->formatted_address = $request->formatted_address;
            $locationData->apt_number = $request->apt_number;
            $locationData->notes = $request->notes;
            $locationData->save();

            return response()->json(["success" => true,"id"=>$locationData->id]);
        } catch (\Exception $e) {
            return response()->json(["success" => false,"msg"=>$e->getMessage()]);

        }


    }

    public function create()
    {
        return view("pip.create");
    }

    public function index()
    {
        return view("pip.index");
    }

    public function details(Request $request)
    {
        $id = (int)$request->id;
        $row = LocationData::find($id);

        return view("pip.details", ["row" => $row,"id"=>$id]);
    }


    public function getLocations(Request $request)
    {
        $pip_name = $request->q;
        $rows = LocationData::where('pip_name', 'like', '%' . Input::get('q') . '%')
            ->take(20)
            ->orderby("pip_name")->get();
        $data = array("incomplete_results" => false, "items" => ($rows), "total_count" => 20);

        return $data;
    }


}
