<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\LocationData
 *
 * @property int $id
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $formatted_address
 * @property string|null $pip_name
 * @property string|null $creator_name
 * @property string|null $created_password
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData whereCreatedPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData whereCreatorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData whereFormattedAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData wherePipName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LocationData whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LocationData extends Model
{
    //
}
