
<!DOCTYPE html>
<html>
<head>
    <title>Pip Location</title>
    <meta name="description" content="Personalized Location Keeper">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <meta name="csrf-token" content="{{csrf_token()}}" />

    <style>
        body {
            color: #333;
        }



        .map_canvas {

            width: 100%;
            left: 0;
            height:300px;



        }

        #multiple li {
            cursor: pointer;
            text-decoration: underline;
        }
    </style>
    <style type="text/css" media="screen">

        ul span { color: #999; }
    </style>
</head>
<body>



<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header navbar-right">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="{{route("pip.index")}}"><img src="logo.png" width=150 style="display: inline-block;" style="
 font-family: 'Montserrat', Arial, sans-serif; font-size: 2.25em; font-weight: bold; color: #000000; text-decoration: none; -webkit-transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important; -moz-transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important; -o-transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important; -ms-transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important; transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important;
 max-width: 350px; max-height: 75px; -webkit-transition: all 180ms linear; -moz-transition: all 180ms linear; -o-transition: all 180ms linear; -ms-transition: all 180ms linear; transition: all 180ms linear; "></a>




        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-left">
                <li style="font-size: .9em; text-decoration: none; text-transform: uppercase; color: inherit; padding: 5px 15px; letter-spacing: 1px;"><a href="{{route("pip.index")}}">Home</a></li>
                <li style="font-size: .9em; text-decoration: none; text-transform: uppercase; color: inherit; padding: 5px 15px; letter-spacing: 1px;"><a href="{{route("pip.create")}}">Create New</a></li>
                <li style="font-size: .9em; text-decoration: none; text-transform: uppercase; color: inherit; padding: 5px 15px; letter-spacing: 1px;"><a href="">Help</a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="container">
    @yield('content')
  </div>


</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>

@stack('scripts')


</body>
</html>

