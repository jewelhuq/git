
<!DOCTYPE html>
<html>
<head>
    <title>Pip Location</title>
    <meta name="description" content="Personalized Location Keeper">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <meta name="csrf-token" content="{{csrf_token()}}" />

    <style>
        body {
            color: #333;
        }



        .map_canvas {

            width: 100%;
            left: 0;
            height:300px;



        }

        #multiple li {
            cursor: pointer;
            text-decoration: underline;
        }
    </style>
    <style type="text/css" media="screen">

        ul span { color: #999; }
    </style>
</head>
<body>



<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header navbar-right">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href=index.php><img src="logo.png" width=150 style="display: inline-block;" style="
 font-family: 'Montserrat', Arial, sans-serif; font-size: 2.25em; font-weight: bold; color: #000000; text-decoration: none; -webkit-transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important; -moz-transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important; -o-transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important; -ms-transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important; transition: all 100ms cubic-bezier(0.55, 0.085, 0.68, 0.53) !important;
 max-width: 350px; max-height: 75px; -webkit-transition: all 180ms linear; -moz-transition: all 180ms linear; -o-transition: all 180ms linear; -ms-transition: all 180ms linear; transition: all 180ms linear; "></a>




        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-left">
                <li style="font-size: .9em; text-decoration: none; text-transform: uppercase; color: inherit; padding: 5px 15px; letter-spacing: 1px;"><a href="index.php"> WHO</a></li>
                <li style="font-size: .9em; text-decoration: none; text-transform: uppercase; color: inherit; padding: 5px 15px; letter-spacing: 1px;"><a href="why.php"> WHY</a></li>
                <li style="font-size: .9em; text-decoration: none; text-transform: uppercase; color: inherit; padding: 5px 15px; letter-spacing: 1px;"><a href="what.php">WHAT</a></li>
                <li style="font-size: .9em; text-decoration: none; text-transform: uppercase; color: inherit; padding: 5px 15px; letter-spacing: 1px;"><a href="where.php">WHERE</a></li>
            </ul>
        </div>
    </div>
</nav>


        <div class="container">

            <form id="pipLocationForm">
            <div class="row">


                <div class="col-md-12">

                    <input id="geocomplete" type="text" placeholder="Type in an address"  class="form-control" class="center-block img-responsive"/>


    <input id="find" type="button" value="find" style="display: none" />

    <div class="map_canvas"></div>
                        <input id="pip_name" name="pip_name"        type="text" placeholder="PIP name"  required="true" class="form-control" class="center-block img-responsive"/>
                        <input id="creator_name"  name="creator_name"    type="text" placeholder="Your name"  class="form-control" class="center-block img-responsive"/>
                        <input id="created_password" name="created_password" type="text" placeholder="Password"  class="form-control" class="center-block img-responsive"/>
                    <div class="col-md-12 text-center">

                    <a class="btn btn-primary" onclick="createPipLocation()" class="center-block img-responsive">Create</a>
                    </div>
                </div>
            </div>

                <input style="display:none" name="lat" type="text" value="">


                <input style="display:none" name="lng" type="text" value="">


                <input name="formatted_address"  style="display:none" type="text" value="">

            </form>
        </div>


</body>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDJ2bLO-zmoOMUiqVwfVptMvV-S3SkuhMA"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script src="js/jquery.geocomplete.js"></script>

<script>
    $(function(){
        $("#geocomplete").geocomplete({
            map: ".map_canvas",
            details: "form ",
            markerOptions: {
                draggable: true
            }
        });

        $("#geocomplete").bind("geocode:dragged", function(event, latLng){
            $("input[name=lat]").val(latLng.lat());
            $("input[name=lng]").val(latLng.lng());
            $("#reset").show();
            console.log(latLng);
        });


        $("#reset").click(function(){
            $("#geocomplete").geocomplete("resetMarker");
            $("#reset").hide();
            return false;
        });

        $("#find").click(function(){
            $("#geocomplete").trigger("geocode");
        }).click();
    });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    function createPipLocation()
    {
        var datastring = $("#pipLocationForm").serialize();
        $.ajax({
            type: "POST",
            url: "{{route("pip.store")}}",
            data: datastring,
            dataType: "json",
            success: function(data) {
                if (data.success) {

                alert("Pip Created");
            }
            else
                {
                    alert('Pip Name already taken by someone');

                }
            },
            error: function() {
                alert('Pip Name already taken by someone');
            }
        });
    }


</script>

</body>
</html>

