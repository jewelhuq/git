@extends('layout.default')
@section("content")
    <form id="pipLocationForm">
        <div class="row">


            <div class="col-md-12">

                <form>
                <select class="js-data-example-ajax" class="form-control" style="width: 95%"></select>
                </form>


            </div>

            @endsection

            @push("scripts")
                <link href="{{asset("css/select2.min.css")}}" rel="stylesheet"/>
                <script src="{{asset("js/select2.min.js")}}"></script>
                <script>
                    $("#pip_name").select2({

                        allowClear: !0,
                        initSelection: function (element, callback) {
                            callback({id: data.id, text: '' + pip_name});
                        },
                        ajax: {
                            url: "{{route("pip.getlocations")}}",
                            dataType: "json",
                            delay: 250,
                            data: function (e) {
                                return {
                                    q: e.term,
                                    page: e.page
                                }
                            },
                            processResults: function (e, t) {
                                return t.page = t.page || 1, {
                                    results: e.items,
                                    pagination: {
                                        more: 30 * t.page < e.total_count
                                    }
                                }
                            },
                            cache: !0
                        },
                        escapeMarkup: function (e) {
                            return e
                        },
                        minimumInputLength: 0,
                        templateResult: function (e) {
                            if (e.loading) return e.text;
                            /**************Modified*****************/
                            var nickname = "";
                            var payee = e.pip_name;
                            var nickname = " <span style='font-weight: normal'> (" + e.creator_name + ")</span>";
                            var final_payee_name = payee + nickname;
                            var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + final_payee_name + "</div>";
                            return e.description && (t += "<div class='select2-result-repository__description'>" + final_payee_name + "</div>"),
                                t += "<div class='select2-result-repository__statistics'><div class='select2-result-repository__forks'><i class=''></i> " + e.nick_name + " </div>" +
                                    "</div></div></div>"
                        },
                        templateSelection: function (e) {

                            /**********************when data will be selected it will work then ***/

                            if (e.id) {


                            }
                            return e.pip_name || e.text
                        }
                    });

                    /***********************END******************/
                    $(".js-data-example-ajax").select2({
                        ajax: {
                            url: "{{route("pip.getlocations")}}",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    q: params.term, // search term
                                    page: params.page
                                };
                            },
                            processResults: function (data, params) {
                                // parse the results into the format expected by Select2
                                // since we are using custom formatting functions we do not need to
                                // alter the remote JSON data, except to indicate that infinite
                                // scrolling can be used
                                params.page = params.page || 1;

                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.total_count
                                    }
                                };
                            },
                            cache: true
                        },
                        placeholder: 'Search for a pip name',
                        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                        minimumInputLength: 1,
                        templateResult: formatRepo,
                        templateSelection: formatRepoSelection
                    });

                    function formatRepo (repo) {
                        if (repo.loading) {
                            return repo.text;
                        }

                        var markup = "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__meta'>" +
                            "<div class='select2-result-repository__title'>" + repo.pip_name + "</div>";

                        if (repo.creator_name) {
                            markup += "<div class='select2-result-repository__description'>" + repo.creator_name +"</div>";
                        }


                        return markup;
                    }

                    function formatRepoSelection (repo) {
                        if(repo.id) {
                            window.location = "{{route("pip.details")}}?id=" + repo.id;
                        }
                            return repo.pip_name || repo.text;

                    }
                </script>

    @endpush