@extends('layout.default')
@section("content")
    <form id="pipLocationForm">
        <div class="row">


            <div class="col-md-12">

                <input id="geocomplete" type="text" placeholder="Type in an address" class="form-control"
                       class="center-block img-responsive"/>


                <input id="find" type="button" value="find" style="display: none"/>

                <div class="map_canvas"></div>
                <input id="pip_name" name="pip_name" type="text" placeholder="PIP name" required="true"     class="form-control" class="center-block img-responsive"/>
                <input id="apt_number" name="apt_number" type="text" placeholder="Apt No"    class="form-control" class="center-block img-responsive"/>
                <input id="notes" name="notes" type="text" placeholder="Special Note"    class="form-control" class="center-block img-responsive"/>

                <input id="creator_name" name="creator_name" type="text" placeholder="Your name" class="form-control"  class="center-block img-responsive"/>
                <input id="created_password" name="created_password" type="text" placeholder="Password"    class="form-control" class="center-block img-responsive"/>

                <div class="col-md-12 text-center">

                    <a class="btn btn-primary" onclick="createPipLocation()"
                       class="center-block img-responsive">Create</a>
                </div>
            </div>
        </div>

        <input style="display:none" name="lat" type="text" value="">


        <input style="display:none" name="lng" type="text" value="">


        <input name="formatted_address" style="display:none" type="text" value="">

    </form>

@endsection


@push("scripts")


    <script src="{{asset("js/jquery.geocomplete.js")}}"></script>

    <script>
    function createPipLocation()
    {
    var datastring = $("#pipLocationForm").serialize();
    $.ajax({
    type: "POST",
    url: "{{route("pip.store")}}",
    data: datastring,
    dataType: "json",
    success: function(data) {
    if (data.success) {

    alert("Pip Created");
    }
    else
    {
    alert('Pip Name already taken by someone');

    }
    },
    error: function() {
    alert('Pip Name already taken by someone');
    }
    });
    }
    </script>

    <script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDJ2bLO-zmoOMUiqVwfVptMvV-S3SkuhMA"></script>

    <script src="{{asset("js/jquery.geocomplete.js")}}"></script>

    <script>
        $(function(){
            $("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form ",
                markerOptions: {
                    draggable: true
                }
            });

            $("#geocomplete").bind("geocode:dragged", function(event, latLng){
                $("input[name=lat]").val(latLng.lat());
                $("input[name=lng]").val(latLng.lng());
                $("#reset").show();
                console.log(latLng);
            });


            $("#reset").click(function(){
                $("#geocomplete").geocomplete("resetMarker");
                $("#reset").hide();
                return false;
            });

            $("#find").click(function(){
                $("#geocomplete").trigger("geocode");
            }).click();
        });


@endpush