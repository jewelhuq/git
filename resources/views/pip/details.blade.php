@extends('layout.default')
@section("content")
    <style>
        #map {
            height: 300px;
            width: 90%;
        }
    </style>

    <form id="pipLocationForm">
        <div class="row">


            <div class="col-md-12">

                <div style="padding:10px">
                    <div id="map"></div>
                </div>


<div align="center">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Share
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="sms:;?&body={{route("pip.details")}}?id={{$id}}">Send by Sms</a></li>
                        <li><a href="mailto:?to=&body={{route("pip.details")}}?id={{$id}}">Email</a></li>
                        <li><a href="whatsapp://send?text={{route("pip.details")}}?id={{$id}}">Whatsapps</a></li>
                    </ul>
                </div>
                </p>

                <p align="center" style="font-weight: bold">"{{$row->pip_name}}"</p>
                <p align="center">{{$row->creator_name}}</p>
                <hr>
                <p align="center">{{$row->formatted_address}}</p>
                <p align="center">Apt # :{{$row->apt_number}}</p>
                @if($row->notes)
                    <p align="center">Note:{{$row->notes}}</p>
                @endif

                @if($row->department)
                    <p align="center">Department:{{$row->department}}</p>
                @endif

<p align="center">

                <a href="https://www.google.com/maps/?q={{$row->latitude}},{{$row->longitude}}" class="btn btn-success">Navigate</a>

</p>

            </div>
        </div>
    </form>


@endsection

@push("scripts")


    <script type="text/javascript">
        var map;

        function initMap() {
            var latitude = {{$row->latitude}}; // YOUR LATITUDE VALUE
            var longitude = {{$row->longitude}}; // YOUR LONGITUDE VALUE

            var myLatLng = {lat: latitude, lng: longitude};

            map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 16
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                //title: 'Hello World'

                // setting latitude & longitude as title of the marker
                // title is shown when you hover over the marker
                title: latitude + ', ' + longitude
            });
        }

        function navigate()
        {
            windo
        }
    </script>



    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJ2bLO-zmoOMUiqVwfVptMvV-S3SkuhMA&callback=initMap"
            async defer></script>

@endpush