@extends('layout.default')
@section("content")

<style>
        .map_canvas {

            width: 100%;
            left: 0;
            height:300px;



        }

        #multiple li {
            cursor: pointer;
            text-decoration: underline;
        }
    </style>
    <style type="text/css" media="screen">

        ul span { color: #999; }
    </style>
    </head>
    <body>





        <form id="pipLocationForm">
            <div class="row">


                <div class="col-md-12">

                    <input id="geocomplete" type="text" placeholder="Type in an address"  class="form-control" class="center-block img-responsive"/>

                    <div class="col-md-12 text-center">

                        <a class="btn btn-success" onclick="getCurrentLocation()" class="center-block img-responsive">Get Current Location</a>
                    </div>



                    <input id="find" type="button" value="find" style="display: none" />

                    <div class="map_canvas"></div>
                    <input id="pip_name" name="pip_name" type="text" placeholder="PIP name" required="true"     class="form-control" class="center-block img-responsive"/>
                    <input id="apt_number" name="apt_number" type="text" placeholder="Apt No"    class="form-control" class="center-block img-responsive"/>
                    <input id="notes" name="notes" type="text" placeholder="Special Note"    class="form-control" class="center-block img-responsive"/>

                    <input id="department" name="department" type="text" placeholder="Department"    class="form-control" class="center-block img-responsive"/>


                    <input id="creator_name" name="creator_name" type="text" placeholder="Your name" class="form-control"  class="center-block img-responsive"/>
                    <input id="created_password" name="created_password" type="text" placeholder="Password"    class="form-control" class="center-block img-responsive"/>
                    <div class="col-md-12 text-center">

                        <a class="btn btn-primary" onclick="createPipLocation()" class="center-block img-responsive">Create</a>
                    </div>
                </div>
            </div>

            <input style="display:none" name="lat" type="text" value="">


            <input style="display:none" name="lng" type="text" value="">


            <input name="formatted_address"  style="display:none" type="text" value="">

        </form>
    </div>


    </body>





@endsection


@push("scripts")


    <script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDJ2bLO-zmoOMUiqVwfVptMvV-S3SkuhMA"></script>

    <script src="{{asset("js/jquery.geocomplete.js")}}"></script>

    <script>
        $(function(){
            $("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form ",
                markerOptions: {
                    draggable: true
                }
            });

            $("#geocomplete").bind("geocode:dragged", function(event, latLng){
                $("input[name=lat]").val(latLng.lat());
                $("input[name=lng]").val(latLng.lng());
                $("#reset").show();
                console.log(latLng);
            });


            $("#reset").click(function(){
                $("#geocomplete").geocomplete("resetMarker");
                $("#reset").hide();
                return false;
            });

            $("#find").click(function(){
                $("#geocomplete").trigger("geocode");
            }).click();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        function createPipLocation()
        {
            var datastring = $("#pipLocationForm").serialize();
            $.ajax({
                type: "POST",
                url: "{{route("pip.store")}}",
                data: datastring,
                dataType: "json",
                success: function(data) {
                    if (data.success)
                    {

                        window.location = "{{route("pip.details")}}?id="+data.id;
                    }
                    else
                    {
                        alert('Pip Name already taken by someone');

                    }
                },
                error: function() {
                    alert('Pip Name already taken by someone');
                }
            });
        }


        function getCurrentLocation()
        {
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    $("#geocomplete").bind("geocode:dragged", function(event, latLng){
                        $("input[name=lat]").val(pos.lat());
                        $("input[name=lng]").val(pos.lng());
                        $("#reset").show();
                        console.log(latLng);
                    });

                    console.log(pos);
                   // infoWindow.setPosition(pos);
                    //infoWindow.setContent('Location found.');
                    //infoWindow.open(map);
                    //map.setCenter(pos);
                }, function() {
                   // handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                //handleLocationError(false, infoWindow, map.getCenter());
            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);



        }

    </script>




@endpush